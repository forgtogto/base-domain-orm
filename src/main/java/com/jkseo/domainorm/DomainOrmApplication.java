package com.jkseo.domainorm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class DomainOrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(DomainOrmApplication.class, args);
    }

}
